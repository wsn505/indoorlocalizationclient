相关函数均不是static的，所以调用前请先产生一个的ClientAPI类的实例
在传输数据前先调用connectTheServer(String serverIP, int port)函数，参数分别为服务器的ＩＰ地址和端口，ＩＰ地址是字符串类型．
传输数据使用sendTheData(ArrayList<Protocol> sendData)函数，请传入通讯协议类的ArrayList.
数据传输完成后调用closeTheConnection()函数关闭套接字连接．

Protocol类即为通讯协议类，内容即原定内容．因为归一化算法尚未确定，所以Protocol类的方法尚未完成，但数据域已经确定，具体请看源码．

另外sendTheData(ArrayList<Protocol> sendData)函数中finally那行始终有警报，还没搞清楚怎么回事，但是应该不影响正常运行．

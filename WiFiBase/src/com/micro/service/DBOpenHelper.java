package com.micro.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper
{
  public DBOpenHelper(Context paramContext)
  {
    super(paramContext, "MyData.db", null, 1);
  }

  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("create table if not exists allPoints(pointid vchar(10) primary key not null, x integer,y integer, pointname vchar(20),macnum integer)");
    paramSQLiteDatabase.execSQL("create table if not exists macIDs(macid integer primary key autoincrement unique, macaddr vchar(20) unique)");
    paramSQLiteDatabase.execSQL("create table if not exists allMacs(macid integer,pointid vchar(10),level integer,maxlevel integer,minlevel integer,times integer,d integer)");
  }

  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
  }
}
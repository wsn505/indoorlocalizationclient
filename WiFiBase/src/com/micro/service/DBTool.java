package com.micro.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import wifiscan.AP;
import wifiscan.MainActivity;
import wifiscan.Point;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DBTool
{
  private HashSet<String> AllPointIDs;
  private DBOpenHelper dbOpenHelper = null;
  private Set<String> totalAPs;

  public DBTool(Context paramContext)
  {
    this.dbOpenHelper = new DBOpenHelper(paramContext);
    init();
  }

  private void creatTables()
  {
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getWritableDatabase();
    localSQLiteDatabase.execSQL("create table if not exists allPoints(pointid vchar(10) primary key not null, x integer,y integer, pointname vchar(20))");
    localSQLiteDatabase.execSQL("create table if not exists macIDs(macid integer primary key autoincrement unique, macaddr vchar(20) unique)");
    localSQLiteDatabase.execSQL("create table if not exists allMacs(macid integer,pointid vchar(10),level integer)");
    localSQLiteDatabase.close();
  }

  private ArrayList<AP> findAPsByPointID(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Cursor localCursor = this.dbOpenHelper.getReadableDatabase().rawQuery("select * from allMacs where pointid=?", new String[] { paramString });
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        return localArrayList;
      }
      localArrayList.add(new AP(localCursor.getInt(localCursor.getColumnIndex("macid")), localCursor.getInt(localCursor.getColumnIndex("level")), localCursor.getInt(localCursor.getColumnIndex("maxlevel")), localCursor.getInt(localCursor.getColumnIndex("minlevel")), localCursor.getInt(localCursor.getColumnIndex("times")), localCursor.getInt(localCursor.getColumnIndex("d"))));
    }
  }

  private Set<String> loadAllMACaddrs()
  {
    Cursor localCursor = this.dbOpenHelper.getReadableDatabase().rawQuery("select macaddr from macIDs", null);
    HashSet localHashSet = new HashSet();
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        return localHashSet;
      }
      localHashSet.add(localCursor.getString(localCursor.getColumnIndex("macaddr")));
    }
  }

  private HashSet<String> loadAllPointIDs()
  {
    Cursor localCursor = this.dbOpenHelper.getReadableDatabase().rawQuery("select pointid from allPoints", null);
    HashSet localHashSet = new HashSet();
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        return localHashSet;
      }
      localHashSet.add(localCursor.getString(localCursor.getColumnIndex("pointid")));
    }
  }

  public void clearAllData()
  {
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getWritableDatabase();
    localSQLiteDatabase.execSQL("drop table if exists macIDs");
    localSQLiteDatabase.execSQL("drop table if exists allPoints");
    localSQLiteDatabase.execSQL("drop table if exists allMacs");
    localSQLiteDatabase.execSQL("create table if not exists allPoints(pointid vchar(10) primary key not null, x integer,y integer, pointname vchar(20),macnum integer)");
    localSQLiteDatabase.execSQL("create table if not exists macIDs(macid integer primary key autoincrement unique, macaddr vchar(20) unique)");
    localSQLiteDatabase.execSQL("create table if not exists allMacs(macid integer,pointid vchar(10),level integer,maxlevel integer,minlevel integer,times integer,d integer)");
    localSQLiteDatabase.close();
  }

  public ArrayList<String> findPointByMacIDandLevel(int paramInt1, int paramInt2, int paramInt3)
  {
    ArrayList localArrayList = new ArrayList();
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getReadableDatabase();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramInt1+"";//
    Cursor localCursor = localSQLiteDatabase.rawQuery("select * from allMacs where macid=?", arrayOfString);
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        return localArrayList;
      }
      String str = localCursor.getString(localCursor.getColumnIndex("pointid"));
      int i = localCursor.getInt(localCursor.getColumnIndex("maxlevel"));
      int j = localCursor.getInt(localCursor.getColumnIndex("minlevel"));
      System.out.println(paramInt1 + "最大强度为:" + i + "<>最小强度为:" + j + "<>该点强度为:" + paramInt2);
      if ((paramInt2 > j - paramInt3) && (paramInt2 < i + paramInt3))
      {
        localArrayList.add(str);
        System.out.println("为可能点");
      }
      else
      {
        System.out.println("不是可能地点");
      }
    }
  }

  public ArrayList<AP> getAPsByID(String paramString)
  {
    return null;
  }

  public HashSet<String> getAllPointIDs()
  {
    return this.AllPointIDs;
  }

  public double getKP(String paramString, int paramInt)
  {
    int i = 0;
    int j = 0;
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getReadableDatabase();
    String[] arrayOfString = new String[2];
    arrayOfString[0] = paramInt+"";
    arrayOfString[1] = paramString;
    Cursor localCursor = localSQLiteDatabase.rawQuery("select * from allMacs where macid=? and pointid=?", arrayOfString);
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        if (i == 0)
          break;
        return j * 10 / MainActivity.SCANTIME;
      }
      j = localCursor.getInt(localCursor.getColumnIndex("times"));
      i = localCursor.getInt(localCursor.getColumnIndex("d"));
    }
    return j * 10 / MainActivity.SCANTIME;
  }

  public int getMACIDbyMAC(String paramString)
  {
    Cursor localCursor = this.dbOpenHelper.getReadableDatabase().rawQuery("select macid from macIDs where macaddr = ?", new String[] { paramString });
    if (localCursor.moveToFirst())
    {
      int i = localCursor.getInt(localCursor.getColumnIndex("macid"));
      localCursor.close();
      return i;
    }
    localCursor.close();
    return -1;
  }

  public ArrayList<Point> getPointsInfoByID(ArrayList<String> paramArrayList)
  {
    ArrayList localArrayList = new ArrayList();
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getReadableDatabase();
    int i = 0;
    if (i >= paramArrayList.size())
      return localArrayList;
    String str = (String)paramArrayList.get(i);
    Point localPoint = new Point(str);
    Cursor localCursor = localSQLiteDatabase.rawQuery("select * from allPoints where pointid=?", new String[] { str });
    while (true)
    {
      if (!localCursor.moveToNext())
      {
        localCursor.close();
        return localArrayList;
      }
      localPoint.setName(localCursor.getString(localCursor.getColumnIndex("pointname")));
      localPoint.setX(localCursor.getInt(localCursor.getColumnIndex("x")));
      localPoint.setY(localCursor.getInt(localCursor.getColumnIndex("y")));
      localPoint.setMacNum(localCursor.getInt(localCursor.getColumnIndex("macnum")));
      localPoint.setAps(findAPsByPointID(str));
      localArrayList.add(localPoint);
    }
  }

  public Set<String> getTotalAPs()
  {
    return this.totalAPs;
  }

  public void init()
  {
    creatTables();
    this.AllPointIDs = loadAllPointIDs();
    this.totalAPs = loadAllMACaddrs();
  }

  public void saveAPs(String paramString)
  {
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getWritableDatabase();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("macaddr", paramString);
    localSQLiteDatabase.insert("macIDs", null, localContentValues);
    localSQLiteDatabase.close();
  }

  public void savePoint(Point paramPoint)
  {
    SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getWritableDatabase();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("pointid", paramPoint.getID());
    localContentValues.put("x", Integer.valueOf(paramPoint.getX()));
    localContentValues.put("y", Integer.valueOf(paramPoint.getY()));
    localContentValues.put("pointname", paramPoint.getName());
    localContentValues.put("macnum", Integer.valueOf(paramPoint.getMacNum()));
    localSQLiteDatabase.insert("allPoints", null, localContentValues);
    localSQLiteDatabase.close();
  }

  public void savePoint_APs(Point paramPoint)
  {
    int i = 0;
    if (i >= paramPoint.getAps().size())
      return;
    int j = getMACIDbyMAC(((AP)paramPoint.getAps().get(i)).getBSSID());
    if (j < 0)
      System.out.println("该点没有被编号");
    else
    {
      SQLiteDatabase localSQLiteDatabase = this.dbOpenHelper.getWritableDatabase();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("macid", Integer.valueOf(j));
      localContentValues.put("pointid", paramPoint.getID());
      localContentValues.put("level", Integer.valueOf(((AP)paramPoint.getAps().get(i)).getLevel()));
      localContentValues.put("minlevel", Integer.valueOf(((AP)paramPoint.getAps().get(i)).getMinlevel()));
      localContentValues.put("maxlevel", Integer.valueOf(((AP)paramPoint.getAps().get(i)).getMaxlevel()));
      localContentValues.put("times", Integer.valueOf(((AP)paramPoint.getAps().get(i)).getTimes()));
      localContentValues.put("d", Integer.valueOf(((AP)paramPoint.getAps().get(i)).getD()));
      localSQLiteDatabase.insert("allMacs", null, localContentValues);
      localSQLiteDatabase.close();
    }
  }
}
package wifiscan;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.micro.service.DBTool;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MainActivity extends Activity
{
  public static int SCANTIME = 10;
  private Button Located_but;
  private Button delAll_but;
  private DBTool dt;
  private Handler handler = new Handler()
{
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 291)
      {
        MainActivity.this.textView.append(MainActivity.this.info);
        MainActivity.this.textView.invalidate();
        MainActivity.this.info = "";
      }
    }
  };
  private String info = "";
  private boolean isGetData = false;
  private Button loadData_but;
  private HashMap<Integer, Integer> scanResult;
  private Button studymod_but;
  private EditText textView;
  private WifiManager wifiManager;

  private double calculate_Distance(Point paramPoint, Map<Integer, Integer> paramMap)//计算相似度
  {
    float f1 = 0.0F;
    HashMap localHashMap = new HashMap();
    int i = 0;
    Iterator localIterator1;
    Iterator localIterator2;
    if (i >= paramPoint.getAps().size())
    {
      localIterator1 = paramMap.keySet().iterator();
      if (localIterator1.hasNext())
        break;
      localIterator2 = localHashMap.keySet().iterator();
    }
    while (true)
    {
      if (!localIterator2.hasNext())
      {
        float f2 = (float)Math.sqrt(f1);
        System.out.println("与" + paramPoint.getID() + "的相似度为：" + f2);
        return f2;
        localHashMap.put(Integer.valueOf(((AP)paramPoint.getAps().get(i)).getMACID()), Float.valueOf((float)(((AP)paramPoint.getAps().get(i)).getLevel() / 10.0D)));
        i++;
        break;
        int j = ((Integer)localIterator1.next()).intValue();
        if (localHashMap.containsKey(Integer.valueOf(j)))
        {
          f1 = (float)(f1 + 4.0D * (Math.abs((((Integer)paramMap.get(Integer.valueOf(j))).intValue() - ((Float)localHashMap.get(Integer.valueOf(j))).floatValue()) * (((Integer)paramMap.get(Integer.valueOf(j))).intValue() - ((Float)localHashMap.get(Integer.valueOf(j))).floatValue()) * (((Integer)paramMap.get(Integer.valueOf(j))).intValue() - ((Float)localHashMap.get(Integer.valueOf(j))).floatValue())) * this.dt.getKP(paramPoint.getID(), j)));
          break label39;
        }
        f1 = (float)(f1 + 1.5D * ((140 + ((Integer)paramMap.get(Integer.valueOf(j))).intValue()) * (140 + ((Integer)paramMap.get(Integer.valueOf(j))).intValue()) * this.dt.getKP(paramPoint.getID(), j)));
        break label39;
      }
      int k = ((Integer)localIterator2.next()).intValue();
      if (!paramMap.containsKey(Integer.valueOf(k)))
        f1 = (float)(f1 + 2.0D * ((150.0F + ((Float)localHashMap.get(Integer.valueOf(k))).floatValue()) * (150.0F + ((Float)localHashMap.get(Integer.valueOf(k))).floatValue()) * this.dt.getKP(paramPoint.getID(), k)));
    }
  }

  protected ArrayList<Point> findLikelyPoints(HashMap<String, Integer> paramHashMap)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList(paramHashMap.entrySet());
    Collections.sort(localArrayList2, new Comparator()
    {
      public int compare(Map.Entry<String, Integer> paramAnonymousEntry1, Map.Entry<String, Integer> paramAnonymousEntry2)
      {
        return ((Integer)paramAnonymousEntry2.getValue()).intValue() - ((Integer)paramAnonymousEntry1.getValue()).intValue();
      }
    });
    int i;
    if (0.7D * localArrayList2.size() < 10.0D)
      i = localArrayList2.size();
    for (int j = 0; ; j++)
    {
      if (j >= localArrayList2.size());
      while (j >= i)
      {
        return this.dt.getPointsInfoByID(localArrayList1);
        i = (int)(0.7D * localArrayList2.size());
        break;
      }
      System.out.println(((Map.Entry)localArrayList2.get(j)).toString());
      localArrayList1.add((String)((Map.Entry)localArrayList2.get(j)).getKey());
    }
  }

  protected ArrayList<Point> getMinDistance(HashMap<String, Integer> paramHashMap, ArrayList<Point> paramArrayList, Map<Integer, Integer> paramMap, int paramInt)
  //所有结果中最有可能的
  {
    double d1 = 1.7976931348623157E+308D;
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= paramArrayList.size())
      return localArrayList;
    Point localPoint = (Point)paramArrayList.get(i);
    int j = ((Integer)paramHashMap.get(localPoint.getID())).intValue();
    System.out.println("出现的次数为" + j);
    System.out.println(localPoint.getID() + "概率为" + j + "/" + localPoint.getMacNum());
    double d2 = calculate_Distance(localPoint, paramMap) * Math.sqrt(Math.sqrt(localPoint.getMacNum() / j));
    System.out.println("与" + localPoint.getID() + "加上概率以后的相似度为" + d2);
    if (d2 < d1)
    {
      d1 = d2;
      localArrayList.clear();
      localArrayList.add((Point)paramArrayList.get(i));
    }
    while (true)
    {
      i++;
      break;
      if (d2 == d1)
        localArrayList.add((Point)paramArrayList.get(i));
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903040);
    this.dt = new DBTool(this);
    this.wifiManager = ((WifiManager)getSystemService("wifi"));
    if (!this.wifiManager.isWifiEnabled())
      this.wifiManager.setWifiEnabled(true);
    this.textView = ((EditText)findViewById(2131230723));
    this.studymod_but = ((Button)findViewById(2131230724));
    this.studymod_but.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MainActivity.this.isGetData = false;
        Intent localIntent = new Intent();
        localIntent.setClass(MainActivity.this, StudyActivity.class);
        MainActivity.this.startActivity(localIntent);
      }
    });
    this.loadData_but = ((Button)findViewById(2131230725));
    this.loadData_but.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MainActivity.this.dt.init();
        Toast.makeText(MainActivity.this, "获取数据", 9000).show();
        MainActivity.this.isGetData = true;
      }
    });
    this.Located_but = ((Button)findViewById(2131230722));
    this.Located_but.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (MainActivity.this.isGetData)
        {
          MainActivity.this.textView.setText("开始定位\n");
          new Thread(new MainActivity.scanWifi(MainActivity.this)).start();
          return;
        }
        Toast.makeText(MainActivity.this, "请点击获取数据按钮", 9000).show();
        System.out.println("请点击获取数据按钮");
      }
    });
    this.delAll_but = ((Button)findViewById(2131230726));
    this.delAll_but.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MainActivity.this.dt.clearAllData();
        MainActivity.this.isGetData = false;
        MainActivity.this.textView.setText("已删除数据库，请进入学习模式采集数据");
        Toast.makeText(MainActivity.this, "清空数据库", 9000).show();
      }
    });
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131165184, paramMenu);
    return true;
  }

  protected void showOnScreen(ArrayList<Point> paramArrayList)//定位结果展示
  {
    this.info = ("找到" + paramArrayList.size() + "个最相近的点:\n");
    for (int i = 0; ; i++)
    {
      if (i >= paramArrayList.size())
        return;
      Point localPoint = (Point)paramArrayList.get(i);
      this.info = (this.info + "地点ID:" + localPoint.getID() + "\n");
      this.info = (this.info + "地点名称:" + localPoint.getName() + "\n");
      this.info = (this.info + "地点X坐标:" + localPoint.getX() + "\n");
      this.info = (this.info + "地点Y坐标:" + localPoint.getY() + "\n");
      this.info += "————————————————\n";
    }
  }

  class scanWifi implements Runnable
  {
    scanWifi()
    {
    }

    private HashMap<String, Integer> countTimes(HashMap<Integer, Integer> paramHashMap)//统计可供选择地相似点
    {
      HashMap localHashMap = new HashMap();
      Iterator localIterator = paramHashMap.keySet().iterator();
      int i = 0;
      int j;
      ArrayList localArrayList;
      int k;
      while (true)
      {
        if (!localIterator.hasNext())
        {
          localHashMap.put("count", Integer.valueOf(i));
          MainActivity.this.info = ("共有" + (-1 + localHashMap.size()) + "个可能地点\n");
          MainActivity.this.handler.sendEmptyMessage(291);
          return localHashMap;
        }
        j = ((Integer)localIterator.next()).intValue();
        localArrayList = MainActivity.this.dt.findPointByMacIDandLevel(j, ((Integer)paramHashMap.get(Integer.valueOf(j))).intValue(), 10);
        k = localArrayList.size();
        if (k != 0)
          break;
        MainActivity.this.info = "找不到可能地点\n";
        MainActivity.this.handler.sendEmptyMessage(291);
      }
      i += k;
      int m = 0;
      label186: String str;
      if (m < k)
      {
        str = (String)localArrayList.get(m);
        double d = MainActivity.this.dt.getKP(str, j);
        System.out.println(j + "在" + str + "的靠谱度为" + d);
        if (d > 5.0D)
        {
          if (!localHashMap.containsKey(str))
            break label312;
          localHashMap.put(str, Integer.valueOf(1 + ((Integer)localHashMap.get(str)).intValue()));
        }
      }
      while (true)
      {
        m++;
        break label186;
        break;
        label312: localHashMap.put(str, Integer.valueOf(1));
      }
    }

    private HashMap<Integer, Integer> getWiFiInfo(int paramInt)//比对结果
    {
      int i = 0;
      HashMap localHashMap1 = new HashMap();
      HashMap localHashMap2 = new HashMap();
      HashMap localHashMap3 = new HashMap();
      MainActivity.this.wifiManager.startScan();
      try
      {
        Thread.sleep(500L);
        if (i >= paramInt)
        {
          Iterator localIterator = localHashMap2.keySet().iterator();
          if (localIterator.hasNext())
            break label389;
          MainActivity.this.info = "扫描结束,开始比对结果……\n";
          MainActivity.this.handler.sendEmptyMessage(291);
          return localHashMap3;
        }
      }
      catch (InterruptedException localInterruptedException1)
      {
        while (true)
        {
          Iterator localIterator;
          localInterruptedException1.printStackTrace();
          i = 0;
          continue;
          MainActivity.this.wifiManager.startScan();
          try
          {
            Thread.sleep(500L);
            List localList = MainActivity.this.wifiManager.getScanResults();
            i++;
            MainActivity.this.info = ("第" + i + "次扫描\n");
            MainActivity.this.handler.sendEmptyMessage(291);
            for (int j = 0; j < localList.size(); j++)
            {
              String str1 = ((ScanResult)localList.get(j)).BSSID;
              int k = ((ScanResult)localList.get(j)).level;
              if ((k <= -35) && (k >= -90))
                //break label263;
            }
          }
          catch (InterruptedException localInterruptedException2)
          {
            while (true)
            {

              localInterruptedException2.printStackTrace();
              continue;
              label263: if (localHashMap2.containsKey(str1))
              {
                localHashMap2.put(str1, Integer.valueOf((k + ((Integer)localHashMap2.get(str1)).intValue()) / 2));
                localHashMap1.put(str1, Integer.valueOf(1 + ((Integer)localHashMap1.get(str1)).intValue()));
              }
              else
              {
                localHashMap2.put(str1, Integer.valueOf(k));
                localHashMap1.put(str1, Integer.valueOf(1));
                System.out.println("添加新MAC地址:" + str1 + "强度:" + k);
              }
            }
          }
          String str2 = (String)localIterator.next();
          int m = ((Integer)localHashMap2.get(str2)).intValue();
          int n = MainActivity.this.dt.getMACIDbyMAC(str2);
          ((Integer)localHashMap1.get(str2)).intValue();
          if (n == -1)
            System.out.println("该热点信息不在数据库中，忽略");
          else
            localHashMap3.put(Integer.valueOf(n), Integer.valueOf(m));
        }
      }
    }

    public void run()
    {
      MainActivity.this.scanResult = getWiFiInfo(5);
      HashMap localHashMap = countTimes(MainActivity.this.scanResult);
      int i = ((Integer)localHashMap.remove("count")).intValue();
      System.out.println("总共出现的次数：" + i);
      ArrayList localArrayList = MainActivity.this.findLikelyPoints(localHashMap);
      MainActivity.this.info = ("共有" + localArrayList.size() + "个权值最大的点\n");
      MainActivity.this.handler.sendEmptyMessage(291);
      if (localArrayList.size() > 1)
      {
        MainActivity.this.showOnScreen(MainActivity.this.getMinDistance(localHashMap, localArrayList, MainActivity.this.scanResult, i));
        MainActivity.this.handler.sendEmptyMessage(291);
        return;
      }
      if (localArrayList.size() == 0)
      {
        System.out.println("没有相似的点");
        return;
      }
      MainActivity.this.showOnScreen(localArrayList);
      MainActivity.this.handler.sendEmptyMessage(291);
    }
  }
}
package wifiscan;

import java.util.ArrayList;
import java.util.Collections;

public class Point
{
  private String ID;
  private int MacNum;
  private ArrayList<AP> aps;
  private String name = "";
  private int x;
  private int y;

  public Point()
  {
    this.aps = new ArrayList();
  }

  public Point(String paramString)
  {
    this.ID = paramString;
  }

  public ArrayList<AP> getAps()
  {
    return this.aps;
  }

  public String getID()
  {
    return this.ID;
  }

  public int getMacNum()
  {
    return this.MacNum;
  }

  public String getName()
  {
    return this.name;
  }

  public int getX()
  {
    return this.x;
  }

  public int getY()
  {
    return this.y;
  }

  public void setAps(ArrayList<AP> paramArrayList)
  {
    this.aps = paramArrayList;
  }

  public void setID(String paramString)
  {
    this.ID = paramString;
  }

  public void setMacNum(int paramInt)
  {
    this.MacNum = paramInt;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setX(int paramInt)
  {
    this.x = paramInt;
  }

  public void setY(int paramInt)
  {
    this.y = paramInt;
  }

  public void sort()
  {
    Collections.sort(this.aps);
  }
}
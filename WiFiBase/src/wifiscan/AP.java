package wifiscan;

public class AP
  implements Comparable<AP>
{
  private String BSSID;
  private int MACID;
  private int d;
  private int level;
  private int maxlevel;
  private int minlevel;
  private int times;

  public AP()
  {
  }

  public AP(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    setMACID(paramInt1);
    this.level = paramInt2;
    this.maxlevel = paramInt3;
    this.minlevel = paramInt4;
    this.times = paramInt5;
    this.d = paramInt6;
  }

  public AP(String paramString, int paramInt)
  {
    this.BSSID = paramString;
    this.level = paramInt;
  }

  public int compareTo(AP paramAP)
  {
    return paramAP.level - this.level;
  }

  public String getBSSID()
  {
    return this.BSSID;
  }

  public int getD()
  {
    return this.d;
  }

  public int getLevel()
  {
    return this.level;
  }

  public int getMACID()
  {
    return this.MACID;
  }

  public int getMaxlevel()
  {
    return this.maxlevel;
  }

  public int getMinlevel()
  {
    return this.minlevel;
  }

  public int getTimes()
  {
    return this.times;
  }

  public void setD(int paramInt)
  {
    this.d = paramInt;
  }

  public void setMACID(int paramInt)
  {
    this.MACID = paramInt;
  }

  public void setMaxlevel(int paramInt)
  {
    this.maxlevel = paramInt;
  }

  public void setMinlevel(int paramInt)
  {
    this.minlevel = paramInt;
  }

  public void setTimes(int paramInt)
  {
    this.times = paramInt;
  }
}
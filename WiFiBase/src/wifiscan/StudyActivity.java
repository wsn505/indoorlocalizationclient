package wifiscan;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.micro.service.DBTool;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class StudyActivity extends Activity
{
  private EditText SSIDfiliter;
  private EditText X;
  private EditText Y;
  private DBTool dt = null;
  private Handler handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 291)
      {
        StudyActivity.this.wifiText.append(StudyActivity.this.info);
        StudyActivity.this.wifiText.invalidate();
        StudyActivity.this.info = "";
      }
    }
  };
  private String info = "";
  private String[] need;
  private EditText pointID;
  private EditText pointName;
  private Button start;
  private Point tempPoint;
  private Button test;
  private Set<String> totalAPs = null;
  private HashSet<String> totalPointsID = null;
  private List<ScanResult> wifiList;
  private WifiManager wifiManager;
  private TextView wifiText;

  protected List<ScanResult> chooseBySSID(String[] paramArrayOfString)
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i >= this.wifiList.size())
      return localArrayList;
    for (int j = 0; ; j++)
    {
      if (j >= paramArrayOfString.length)
      {
    	  return localArrayList;//
      }
      if (((ScanResult)this.wifiList.get(i)).SSID.equals(paramArrayOfString[j]))
        localArrayList.add((ScanResult)this.wifiList.get(i));
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903041);
    this.X = ((EditText)findViewById(2131230732));
    this.Y = ((EditText)findViewById(2131230733));
    this.pointID = ((EditText)findViewById(2131230723));
    this.pointName = ((EditText)findViewById(2131230728));
    this.SSIDfiliter = ((EditText)findViewById(2131230735));
    this.wifiText = ((TextView)findViewById(2131230737));
    this.dt = new DBTool(this);
    this.totalPointsID = this.dt.getAllPointIDs();
    this.totalAPs = this.dt.getTotalAPs();
    this.wifiManager = ((WifiManager)getSystemService("wifi"));
    this.test = ((Button)findViewById(2131230738));
    this.test.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        StudyActivity.this.need = StudyActivity.this.SSIDfiliter.getText().toString().trim().split(",");
        StudyActivity.this.wifiManager.startScan();
        StudyActivity.this.wifiList = StudyActivity.this.wifiManager.getScanResults();
        StudyActivity.this.wifiText.setText("");
        if (StudyActivity.this.need[0] != "")
          StudyActivity.this.wifiList = StudyActivity.this.chooseBySSID(StudyActivity.this.need);
        StudyActivity.this.wifiText.append("共找到" + StudyActivity.this.wifiList.size() + "个热点\n");
        for (int i = 0; ; i++)
        {
          if (i >= StudyActivity.this.wifiList.size())
            return;
          StudyActivity.this.wifiText.append(i + 1 + ". " + ((ScanResult)StudyActivity.this.wifiList.get(i)).SSID + "\t" + ((ScanResult)StudyActivity.this.wifiList.get(i)).level + "\n");
        }
      }
    });
    this.start = ((Button)findViewById(2131230736));
    this.start.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        StudyActivity.this.tempPoint = new Point();
        StudyActivity.this.tempPoint.getAps().clear();
        StudyActivity.this.tempPoint.setX(-1);
        StudyActivity.this.tempPoint.setY(-1);
        StudyActivity.this.need = StudyActivity.this.SSIDfiliter.getText().toString().trim().split(",");
        String str1 = StudyActivity.this.pointID.getText().toString().trim();
        if (str1.equals(""))
        {
          Toast.makeText(StudyActivity.this, "请输入地点ID", 9000).show();
          return;
        }
        if (StudyActivity.this.totalPointsID.contains(str1))
        {
          Toast.makeText(StudyActivity.this, "重复的地点ID", 9000).show();
          return;
        }
        StudyActivity.this.tempPoint.setID(str1);
        String str2 = StudyActivity.this.X.getText().toString().trim();
        if (str2 != "");
        try
        {
          StudyActivity.this.tempPoint.setX(Integer.valueOf(str2).intValue());
          label206: String str3 = StudyActivity.this.Y.getText().toString().trim();
          if (str3 != "");
          try
          {
            StudyActivity.this.tempPoint.setY(Integer.valueOf(str3).intValue());
            label251: String str4 = StudyActivity.this.pointName.getText().toString().trim();
            if (str4 != "");
            try
            {
              StudyActivity.this.tempPoint.setName(str4);
              label290: StudyActivity.this.wifiManager.startScan();
              StudyActivity.this.wifiText.setText("\nStarting Scan...\n");
              new Thread(new StudyActivity.scanWifi(StudyActivity.this, MainActivity.SCANTIME)).start();
              return;
            }
            catch (Exception localException1)
            {
              break label290;
            }
          }
          catch (Exception localException2)
          {
            break label251;
          }
        }
        catch (Exception localException3)
        {
          break label206;
        }
      }
    });
  }

  class scanWifi
    implements Runnable
  {
    private int times;

    scanWifi(int arg2)
    {
      int i=0;
      this.times = i;
    }

    private int getDX(List<Integer> paramList, int paramInt)
    {
      float f = 0.0F;
      for (int i = 0; ; i++)
      {
        if (i >= paramList.size())
          return (int)(f / paramList.size());
        f = (float)(f + (((Integer)paramList.get(i)).intValue() - paramInt / 10.0D) * (((Integer)paramList.get(i)).intValue() - paramInt / 10.0D));
      }
    }

    private HashMap<String, Integer> getUseInfo(List<Integer> paramList)
    {
      HashMap localHashMap = new HashMap();
      int i = 0;
      int j = 0;
      int k = 0;
      int m = 0;
      if (m >= paramList.size())
      {
        localHashMap.put("min", Integer.valueOf(j));
        localHashMap.put("max", Integer.valueOf(i));
        localHashMap.put("mid", Integer.valueOf(k * 10 / paramList.size()));
        return localHashMap;
      }
      int n = ((Integer)paramList.get(m)).intValue();
      if (paramList.size() == MainActivity.SCANTIME)
        System.out.println("强度：" + n);
      k += n;
      if (m == 0)
      {
        j = n;
        i = n;
      }
      while (true)
      {
        m++;
        break;
        if (i < n)
          i = n;
        if (j > n)
          j = n;
      }
    }

    private void getWiFiInfo(int paramInt)
    {
      HashMap localHashMap1 = new HashMap();
      int i = 0;
      int j = 0;
      Iterator localIterator;
      if (i >= 3)
      {
        if (j < paramInt)
          break label334;
        localIterator = localHashMap1.keySet().iterator();
        label35: if (localIterator.hasNext())
          break label582;
        StudyActivity.this.totalPointsID.add(StudyActivity.this.tempPoint.getID());
        StudyActivity.this.tempPoint.setMacNum(StudyActivity.this.tempPoint.getAps().size());
        StudyActivity.this.dt.savePoint(StudyActivity.this.tempPoint);
        StudyActivity.this.dt.savePoint_APs(StudyActivity.this.tempPoint);
        localHashMap1.clear();
        System.out.println("保存该点");
        StudyActivity localStudyActivity1 = StudyActivity.this;
        String str3 = localStudyActivity1.info;
        StringBuilder localStringBuilder1 = new StringBuilder(String.valueOf(str3));
        localStudyActivity1.info = ("共找到" + StudyActivity.this.tempPoint.getAps().size() + "个热点\n");
        StudyActivity.this.handler.sendEmptyMessage(291);
      }
      for (int i5 = 0; ; i5++)
      {
        while (true)
        {
          while (true)
          {
            if (i5 < StudyActivity.this.tempPoint.getAps().size())
              break label887;
            StudyActivity localStudyActivity3 = StudyActivity.this;
            String str5 = localStudyActivity3.info;
            StringBuilder localStringBuilder3 = new StringBuilder(String.valueOf(str5));
            localStudyActivity3.info = "扫描结束，记录的节点信息：\n";
            StudyActivity.this.handler.sendEmptyMessage(291);
            return;
            StudyActivity.this.wifiManager.startScan();
            try
            {
              Thread.sleep(500L);
              StudyActivity.this.wifiManager.getScanResults();
              i++;
            }
            catch (InterruptedException localInterruptedException1)
            {
              while (true)
                localInterruptedException1.printStackTrace();
            }
          }
          label334: StudyActivity.this.wifiManager.startScan();
          try
          {
            Thread.sleep(1000L);
            List localList = StudyActivity.this.wifiManager.getScanResults();
            j++;
            if (StudyActivity.this.need[0] != "")
              localList = StudyActivity.this.chooseBySSID(StudyActivity.this.need);
            StudyActivity.this.info = ("第" + j + "次扫描\n");
            StudyActivity.this.handler.sendEmptyMessage(291);
            for (int k = 0; k < localList.size(); k++)
            {
              str1 = ((ScanResult)localList.get(k)).BSSID;
              m = ((ScanResult)localList.get(k)).level;
              if ((m <= -35) && (m >= -95))
                break label518;
            }
          }
          catch (InterruptedException localInterruptedException2)
          {
            while (true)
            {
              String str1;
              int m;
              localInterruptedException2.printStackTrace();
              continue;
              label518: if (localHashMap1.containsKey(str1))
              {
                ((List)localHashMap1.get(str1)).add(Integer.valueOf(m));
              }
              else
              {
                ArrayList localArrayList = new ArrayList();
                localArrayList.add(Integer.valueOf(m));
                localHashMap1.put(str1, localArrayList);
              }
            }
          }
        }
        label582: String str2 = (String)localIterator.next();
        System.out.println("MAC：" + str2);
        HashMap localHashMap2 = getUseInfo((List)localHashMap1.get(str2));
        int n = ((Integer)localHashMap2.get("min")).intValue();
        int i1 = ((Integer)localHashMap2.get("max")).intValue();
        int i2 = ((Integer)localHashMap2.get("mid")).intValue();
        int i3 = ((List)localHashMap1.get(str2)).size();
        int i4 = getDX((List)localHashMap1.get(str2), i2);
        System.out.println("min:" + n + "\tmax:" + i1 + "\tmid:" + i2 / 10.0D + "\ttime:" + i3 + "\td:" + i4);
        AP localAP = new AP(str2, i2);
        localAP.setMaxlevel(i1);
        localAP.setMinlevel(n);
        localAP.setTimes(i3);
        localAP.setD(i4);
        StudyActivity.this.tempPoint.getAps().add(localAP);
        if (StudyActivity.this.totalAPs.contains(str2))
          break label35;
        StudyActivity.this.totalAPs.add(str2);
        StudyActivity.this.dt.saveAPs(str2);
        break label35;
        label887: StudyActivity localStudyActivity2 = StudyActivity.this;
        String str4 = localStudyActivity2.info;
        StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(str4));
        localStudyActivity2.info = (i5 + 1 + ". " + ((AP)StudyActivity.this.tempPoint.getAps().get(i5)).getBSSID() + "\t" + ((AP)StudyActivity.this.tempPoint.getAps().get(i5)).getLevel() / 10.0D + "\n");
      }
    }

    public void run()
    {
      getWiFiInfo(this.times);
    }
  }
}
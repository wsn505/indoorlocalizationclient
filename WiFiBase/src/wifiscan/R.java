package wifiscan;

public final class R
{
  public static final class attr
  {
  }

  public static final class dimen
  {
    public static final int activity_horizontal_margin = 2130968576;
    public static final int activity_vertical_margin = 2130968577;
  }

  public static final class drawable
  {
    public static final int ic_launcher = 2130837504;
  }

  public static final class id
  {
    public static final int DB_but = 2131230725;
    public static final int X = 2131230730;
    public static final int Y = 2131230731;
    public static final int action_settings = 2131230739;
    public static final int clear_but = 2131230726;
    public static final int editPointName = 2131230728;
    public static final int editSSIDS = 2131230735;
    public static final int editView = 2131230723;
    public static final int editX = 2131230732;
    public static final int editY = 2131230733;
    public static final int loc_but = 2131230722;
    public static final int scrollView1 = 2131230721;
    public static final int start = 2131230736;
    public static final int study_but = 2131230724;
    public static final int test_but = 2131230738;
    public static final int textView1 = 2131230720;
    public static final int textView2 = 2131230727;
    public static final int textView3 = 2131230729;
    public static final int textView5 = 2131230734;
    public static final int wifi_info = 2131230737;
  }

  public static final class layout
  {
    public static final int activity_main = 2130903040;
    public static final int learnmod = 2130903041;
  }

  public static final class menu
  {
    public static final int main = 2131165184;
  }

  public static final class string
  {
    public static final int action_settings = 2131034113;
    public static final int app_name = 2131034112;
    public static final int hello_world = 2131034114;
  }

  public static final class style
  {
    public static final int AppBaseTheme = 2131099648;
    public static final int AppTheme = 2131099649;
  }
}
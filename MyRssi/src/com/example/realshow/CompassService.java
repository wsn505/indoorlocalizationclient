/**
 *
 */
package com.example.realshow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import clientNetworkAPI.BuildDbAPI;
import clientNetworkAPI.MatchAPI;
import clientNetworkAPI.Rssfingers;

import com.example.myrssi.R;
import com.example.myrssi.WifiListActivity;
import com.example.utils.Alert;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class CompassService extends Service implements SensorEventListener
{
	Timer timer; 
	MyReceiver serviceReceiver;
//	Bundle bundle;
//	 bundle = new Bundle();  
//     bundle.putString("name", "Tom");  
//     bundle.putInt("age", 4);  
//     intent.putExtras(bundle); 
	 // 定义Sensor管理器
	SensorManager mSensorManager;
	// 定义onBinder方法所返回的对象
	// 必须实现的方法，绑定该Service时回调该方法
	
	
    public int SCANTIMES = 6;
    public String mssid="SmartTravel";
    
    private WifiManager wifiManager;  
    public List<ScanResult> list; 
    
    public Rssfingers rssfingers;//结构
    public ArrayList<Rssfingers> sendData = new ArrayList<Rssfingers>() ;
    
	public EditText inputID;
	public Button scan,upload,clear;
	
	MatchAPI server= new MatchAPI();//server
    public String ip;
    public int port;
    
    Alert alter=new Alert();
    String location_XY;
	Float x_off=new Float(0);
	Float y_off=new Float(0);
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}
	// Service被创建时回调该方法。
	@Override
	public void onCreate()
	{
		super.onCreate();
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_UI);
		serviceReceiver = new MyReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(locationViewActivity.CTL_ACTION);
		registerReceiver(serviceReceiver, filter);
		timer = new Timer();
		timer.schedule( new TimerTask()
		{

			@Override
			public void run()
			{
		    	wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		    	rssfingers = new Rssfingers();//初始化
	
				if (!wifiManager.isWifiEnabled()) 
				{  
			        wifiManager.setWifiEnabled(true); 
			    }    
		        for(int num = 0 ;num<SCANTIMES ;num++)
		        {
		    		list = wifiManager.getScanResults();
		    		wifiManager.startScan();
		        	Thread scanThread = new Thread(new StartScan(mssid));
		        	scanThread.start();
		        	try
		        	{
						Thread.sleep(1000);

					} 
		        	catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	try 
		        	{
						scanThread.join();
					} catch (InterruptedException e) 
		        	{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		        rssfingers.pointId = -1;
	//	        Log.v("Myy",String.valueOf(rssfingers.addrRss));
		        sendData.add(rssfingers);
		        ip="10.13.32.141";
		        port = 8888;
				new Thread(new Runnable(){  
	                @Override  
	                public void run() {  
	    		        try
	    		        {
	    			        server.connectTheServer(ip,port);
	    					Log.v("Myy","service is started");	    			        
	    			        location_XY = server.sendTheData(sendData);
	    			        Alert.ShowToast(CompassService.this, location_XY, 1);
	    			        Log.v("Location",location_XY);
	    			        if(location_XY != null && location_XY != "")
	    			        	handler.sendEmptyMessage(1);
	    			        else
	    			        	handler.sendEmptyMessage(-1);
	    		        }
	    		        catch(Exception e)
	    		        {
	    		        	e.printStackTrace();
	    		        	handler.sendEmptyMessage(-2);
	    		        }  
	                }  
				}).start(); 
			}
		}, 0, 5 * 1000 );
	}
		    
	class StartScan implements Runnable 
	{  
	   public String mSSid;
	   public StartScan(String ssid)
	   {
		   mSSid = ssid;
	   }
	   private void caculateWifi()
	   {
	      for(Iterator<ScanResult> result = list.iterator();result.hasNext(); )
	      {
	         ScanResult scanresult = result.next();
			 if(!scanresult.SSID.equals(mSSid))
	         continue;
	         if(!rssfingers.addrRss.containsKey(scanresult.BSSID))
	         {
	        	 rssfingers.addrRss.put(scanresult.BSSID,new ArrayList<Integer>());
	         }
	             rssfingers.addrRss.get(scanresult.BSSID).add(new Integer(scanresult.level+96));
	       }
	    }
	   @Override
	    public void run()
	    {
	    	caculateWifi();
	    }
	 }
	    
	Handler handler = new Handler() {
			@Override
	public void handleMessage(Message msg) {

		if (msg.what == 1) {
			 StringTokenizer st = new StringTokenizer(location_XY,";",false);
			 x_off.parseFloat(st.nextToken());
			 y_off.parseFloat(st.nextToken());
			 Intent sendIntent = new Intent(locationViewActivity.UPDATE_ACTION);
			 sendIntent.putExtra("X_OFF",x_off );
			 sendIntent.putExtra("Y_OFF",y_off);
			 sendIntent.putExtra("Degree", 0);//可以删除
			// 发送广播 ，将被Activity组件中的BroadcastReceiver接收到
			sendBroadcast(sendIntent);
		sendData.clear();
				}		
				if (msg.what == -1) {
					alter.ShowToast(CompassService.this, "返回location_XY错误", R.drawable.cuohao);
				}
				if (msg.what == -2) {
					alter.ShowToast(CompassService.this, "上传失败", R.drawable.cuohao);
				}
			}
		};
	
	
	@Override
	public void onSensorChanged(SensorEvent event)
	{ 
		// 获取触发event的传感器类型
		int sensorType = event.sensor.getType();
		switch (sensorType)
		{
				case Sensor.TYPE_ORIENTATION:
				// 获取绕Z轴转过的角度。
				float degree = event.values[0];
				 Intent sendIntent = new Intent(locationViewActivity.UPDATE_ACTION);
				 sendIntent.putExtra("X_OFF",x_off );
				 sendIntent.putExtra("Y_OFF",y_off);
				 sendIntent.putExtra("Degree", 0);//可以删除
				// 发送广播 ，将被Activity组件中的BroadcastReceiver接收到
				sendBroadcast(sendIntent);;
				break;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{
	}
	
	
	public class MyReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(final Context context, Intent intent)
		{

		}
	}

}
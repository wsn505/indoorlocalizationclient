/*
 * locationView.java
 * By: Michael Ortiz
 * Updated By: Patrick Lackemacher
 * -------------------
 * Extends Android ImageView to include pinch zooming and panning.
 */

package com.example.realshow;

import java.util.Timer;
import java.util.TimerTask;

import com.example.myrssi.R;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class locationView extends ImageView {

	float x_down = 0;
	float y_down = 0;
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	float oldRotation = 0;
	Matrix matrix = new Matrix();
	Matrix matrix1 = new Matrix();
	Matrix savedMatrix = new Matrix();
	
	float OffsetX = 0;
	float OffsetY = 0;
	float scale_all=1;
	float X_all = 0;
	float Y_all = 0;
	
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	int mode = NONE;

	boolean matrixCheck = false;

	public int screenWidth,screenHeight; 
	public int widthPixels;
	public int heightPixels;

	Bitmap indoorMap;
    private Handler handler = new  Handler()
    {
    	@Override
    	public void handleMessage(Message msg) 
    	{
    		if(msg.what == 1)
    		{
    			invalidate();
    		}
    	}
    };
    
	public locationView(locationViewActivity activity) {
		super(activity);
		indoorMap = BitmapFactory.decodeResource(getResources(), R.drawable.map);
		WindowManager windowManager = (WindowManager) getContext()
        .getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();  
		screenWidth = display.getWidth();  
		screenHeight = display.getHeight();
		//获取这个图片的宽和高 
		int width = indoorMap .getWidth(); 
		int height = indoorMap .getHeight(); 
		//计算缩放率，新尺寸除原始尺寸 
		float scaleWidth = 1;
		float scaleHeight = 1;
		 if (this.getResources().getConfiguration().orientation  == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
		    { 
   				Log.v("Myy","竖屏");
		     //改为竖屏的layout 
				scaleWidth = ((float) screenWidth) / width;
				
				scaleHeight = scaleWidth;//((float) screenHeight) / height;

		   } 
		 else
		 {
				Log.v("Myy","横屏");
				scaleWidth = ((float) screenWidth) / width;
				scaleHeight = scaleWidth;//((float) screenHeight) / height;
		 }	
		// 创建操作图片用的matrix对象 
		matrix = new Matrix(); 
		matrix.postScale(scaleWidth, scaleHeight); 
		
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				Timer timer = new Timer();
//				TimerTask task = new TimerTask(){
//				        public void run()
//				        {
//				        	OffsetX+=180;
//				        	OffsetY+=180;
//			    			Log.v("Myy","thread start");
//				             Message message = new Message();
//				             message.what = 1;
//				             handler.sendMessage(message);
//				        }
//				};
//				timer.schedule(task,10000,10000);	
//			}
//		}).start();//传信息给handle
	}


	protected void onDraw(Canvas canvas) {
		canvas.save();
		//
		locationViewActivity.x_off=2;
		locationViewActivity.y_off=2;
		OffsetX=screenWidth/2-locationViewActivity.x_off*screenWidth/11;
		OffsetY=screenHeight/2-locationViewActivity.y_off*screenWidth/11;
		canvas.drawColor(Color.WHITE);
		canvas.translate(OffsetX , OffsetY);
		canvas.drawBitmap(indoorMap, matrix, null);
		canvas.translate(-OffsetX, -OffsetY);
		Bitmap watermark = BitmapFactory.decodeResource(getResources(), R.drawable.mylocation);
		Matrix waterM = new Matrix();
		waterM.setRotate(locationViewActivity.degree, (float) watermark.getWidth() / 2, (float) watermark.getHeight() / 2);
		Bitmap mwatermark = Bitmap.createBitmap(watermark, 0, 0, watermark.getWidth(),watermark.getHeight(), waterM, true);
		canvas.drawBitmap(mwatermark,(screenWidth-watermark.getWidth())/2,(screenHeight-watermark.getHeight())/2,null);
		canvas.restore();
//		OffsetX-=X_all;
//		OffsetY-=Y_all;
	}

	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			mode = DRAG;
			x_down = event.getX();
			y_down = event.getY();
			savedMatrix.set(matrix);
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			mode = ZOOM;
			oldDist = spacing(event);
			//oldRotation = rotation(event);//捕捉双点旋转
			savedMatrix.set(matrix);
			midPoint(mid, event);
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == ZOOM) {
				matrix1.set(savedMatrix);
				//float rotation = rotation(event) - oldRotation;
				float newDist = spacing(event);
				float scale = newDist / oldDist;
				matrix1.postScale(scale, scale, mid.x, mid.y);// 縮放
				//matrix1.postRotate(rotation, mid.x, mid.y);// 旋轉
				matrixCheck = matrixCheck();
				if (matrixCheck == false) {
					scale_all  = scale_all*scale;//获得去全部变换累计
					matrix.set(matrix1);
					invalidate();
				}
			} else if (mode == DRAG) { 
				matrix1.set(savedMatrix);
				matrix1.postTranslate(event.getX() - x_down, event.getY()
						- y_down);// 平移
				matrixCheck = matrixCheck();
				if (matrixCheck == false) {
					X_all=event.getX() - x_down;//获得去全部变换累计
					Y_all=event.getY()- y_down;//获得去全部变换累计
					matrix.set(matrix1);
					invalidate();
				}
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			break;
		}
		return true;
	}

	private boolean matrixCheck() {
		float[] f = new float[9];
		matrix1.getValues(f);
		// 图片4个顶点的坐标
		float x1 = f[0] * 0 + f[1] * 0 + f[2];
		float y1 = f[3] * 0 + f[4] * 0 + f[5];
		float x2 = f[0] * indoorMap.getWidth() + f[1] * 0 + f[2];
		float y2 = f[3] * indoorMap.getWidth() + f[4] * 0 + f[5];
		float x3 = f[0] * 0 + f[1] * indoorMap.getHeight() + f[2];
		float y3 = f[3] * 0 + f[4] * indoorMap.getHeight() + f[5];
		float x4 = f[0] * indoorMap.getWidth() + f[1] * indoorMap.getHeight() + f[2];
		float y4 = f[3] * indoorMap.getWidth() + f[4] * indoorMap.getHeight() + f[5];
		// 图片现宽度
		double width = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		// 缩放比率判断
		if (width < screenWidth / 3 || width > screenWidth * 3) {
			return true;
		}
		// 出界判断
		if ((x1 < screenWidth / 3 && x2 < screenWidth / 3
				&& x3 < screenWidth / 3 && x4 < screenWidth / 3)
				|| (x1 > screenWidth * 2 / 3 && x2 > screenWidth * 2 / 3
						&& x3 > screenWidth * 2 / 3 && x4 > screenWidth * 2 / 3)
				|| (y1 < screenHeight / 3 && y2 < screenHeight / 3
						&& y3 < screenHeight / 3 && y4 < screenHeight / 3)
				|| (y1 > screenHeight * 2 / 3 && y2 > screenHeight * 2 / 3
						&& y3 > screenHeight * 2 / 3 && y4 > screenHeight * 2 / 3)) {
			return true;
		}
		return false;
	}

	// 触碰两点间距离
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}
	
	// 取手势中心点
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	// 取旋转角度
	private float rotation(MotionEvent event) {
		double delta_x = (event.getX(0) - event.getX(1));
		double delta_y = (event.getY(0) - event.getY(1));
		double radians = Math.atan2(delta_y, delta_x);
		return (float) Math.toDegrees(radians);
	}

	// 将移动，缩放以及旋转后的图层保存为新图片
	// 本例中沒有用到該方法，需要保存圖片的可以參考
	public Bitmap CreatNewPhoto() {
		Bitmap bitmap = Bitmap.createBitmap(screenWidth, screenHeight,
				Config.ARGB_8888); // 背景图片
		Canvas canvas = new Canvas(bitmap); // 新建画布
		canvas.drawBitmap(indoorMap, matrix, null); // 画图片
		canvas.save(Canvas.ALL_SAVE_FLAG); // 保存画布
		canvas.restore();
		return bitmap;
	}

}

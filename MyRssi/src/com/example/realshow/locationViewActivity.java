package com.example.realshow;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class locationViewActivity extends Activity {
    /** Called when the activity is first created. */
	ActivityReceiver activityReceiver;
	public static final String CTL_ACTION = //上传待用
			"com.example.action.CTL_ACTION";
	public static final String UPDATE_ACTION =//通过service更新数据
			"com.example.action.UPDATE_ACTION";
	static float currentDegree;
	static float degree;
	static float x_off,y_off;
	static locationView img;
	@Override 
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        activityReceiver = new ActivityReceiver();
		// 创建IntentFilter
		IntentFilter filter = new IntentFilter();
		// 指定BroadcastReceiver监听的Action
		filter.addAction(UPDATE_ACTION);
		// 注册BroadcastReceiver
		registerReceiver(activityReceiver, filter);
		//创建启动Service的Intent
		final Intent intent= new Intent(this, CompassService.class);
		// 启动后台Service
		startService(intent);
		degree = 0;
		currentDegree = 0;
		img = new locationView(locationViewActivity.this);
		setContentView(img);
    }
//	public void paintLocation() 
//	{
////		Toast.makeText(locationViewActivity.this,
////				"Serivce的count值为：" + binder.getCount(),
////				Toast.LENGTH_SHORT).show();
//	}
	
	public class ActivityReceiver extends BroadcastReceiver//// 自定义的BroadcastReceiver，负责监听从Service传回来的广播
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
//			// 获取Intent中的update消息，update代表播放状态
			degree = -intent.getFloatExtra("Degree", -1);
			 x_off = intent.getFloatExtra("X_OFF", -1);
			 y_off = intent.getFloatExtra("Y_OFF", -1);
//			// 获取Intent中的current消息，current代表当前正在播放的歌曲
		}
	}
	@Override
	public void onDestroy()
	{
		final Intent intent = new Intent();
		// 为Intent设置Action属性
		intent.setAction(CTL_ACTION);
		super.onDestroy();
		stopService(intent); 
	}
}



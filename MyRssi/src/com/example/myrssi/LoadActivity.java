package com.example.myrssi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

public class LoadActivity extends Activity{
	     
	     private static final int LOAD_DISPLAY_TIME = 1500;
	    
	     @SuppressWarnings("deprecation")
		@Override
	     public void onCreate(Bundle savedInstanceState) {
	         super.onCreate(savedInstanceState);
	         
	         
	       getWindow().setFormat(PixelFormat.RGBA_8888);
	       getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);
	
	       setContentView(R.layout.activity_load);
	        new Handler().postDelayed(new Runnable() {
	            public void run() {
	                Intent mainIntent = new Intent(LoadActivity.this, MainActivity.class);
	                LoadActivity.this.startActivity(mainIntent);
	                LoadActivity.this.finish();
             }
	        }, LOAD_DISPLAY_TIME); 
	
	    }
	}

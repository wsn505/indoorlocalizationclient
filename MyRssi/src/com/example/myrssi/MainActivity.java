package com.example.myrssi;

import com.example.myrssi.MainActivity;
import com.example.myrssi.WifiListActivity;
import com.example.realshow.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener //不集成接口报错
{

	Button bn1,bn2,bn3;
	EditText mssid;
	String param;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		bn1 = (Button)findViewById(R.id.button1);
		bn2 = (Button)findViewById(R.id.button2);
		bn3 = (Button)findViewById(R.id.button3);
		bn1.setOnClickListener(this);
		bn2.setOnClickListener(this);
		bn3.setOnClickListener(this);
		mssid = (EditText)findViewById(R.id.editText1);
	}

	
	public void onClick(View v)
	{
		
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.button1:
				Intent in1 = new Intent(MainActivity.this,WifiListActivity.class);
				String param = mssid.getText().toString();
				in1.putExtra("mSSID", param);
				startActivity(in1);
				break;
			case R.id.button2:
				Intent in2 = new Intent(MainActivity.this,locationViewActivity.class);
				startActivity(in2);
				break;
		}
	}

	
}

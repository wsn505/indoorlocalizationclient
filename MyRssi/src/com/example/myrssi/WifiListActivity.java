package com.example.myrssi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;

import clientNetworkAPI.*;
import com.example.utils.*;

public class WifiListActivity extends Activity implements OnClickListener
{  
    public int SCANTIMES = 30;  //how to identity the scantimes
    public String mssid;
    
    private WifiManager wifiManager;  
    public List<ScanResult> list; 
    
    public Rssfingers rssfingers;//结构
    public ArrayList<Rssfingers> sendData = new ArrayList<Rssfingers>() ;
    
	public EditText inputID;
	String strinputID;
	public Button upload,clear,scan;
	
	BuildDbAPI server= new BuildDbAPI();//server
    public String ip;
    public int port;
    
    String fingerId;
    @Override  
    protected void onCreate(Bundle savedInstanceState)
    {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.train_scan); 
        Intent mlastIntent = getIntent();
        mssid= mlastIntent.getStringExtra("mSSID");//获得调用它的activity所传参数
        inputID = (EditText)findViewById(R.id.inputID);
		scan = (Button)findViewById(R.id.scan);
		upload = (Button)findViewById(R.id.upload);
		clear = (Button)findViewById(R.id.clear);
		scan.setOnClickListener(this);
		upload.setOnClickListener(this);
		clear.setOnClickListener(this);
    	Toast toast = Toast.makeText(this, "过滤的SSID为："+mssid+"，等待正在初始化。。。", 1000);
    	toast.setGravity(Gravity.CENTER, 0, 0);
    	toast.show();
    }  
    
    class StartScan implements Runnable 
    {  
    	public String mSSid;
    	public StartScan(String ssid)
    	{
    		mSSid = ssid;
    	}
    	private void caculateWifi()
    	{
            for(Iterator<ScanResult> result = list.iterator();result.hasNext(); )
            {
            	ScanResult scanresult = result.next();
				if(!scanresult.SSID.equals(mSSid))
            		continue;
            	if(!rssfingers.addrRss.containsKey(scanresult.BSSID))
            	{
            		rssfingers.addrRss.put(scanresult.BSSID,new ArrayList<Integer>());
            	}
            	rssfingers.addrRss.get(scanresult.BSSID).add(new Integer(scanresult.level+96));
            }
    	}
    	@Override
    	public void run()
    	{
    		caculateWifi();
    	}
    }
        
    public void onClick(View v)
	{
		
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.scan:
				 strinputID= inputID.getText().toString();
				if( !(Integer.parseInt(strinputID)>0&&Integer.parseInt(strinputID)<1000*1000))//可在添加id判重
				{
			    	Toast toast = Toast.makeText(this, "过滤的SSID为："+mssid+"，等待正在初始化。。。", 1000);
			    	toast.setGravity(Gravity.CENTER, 0, 0);
			    	toast.show();
			    	break;
				}
		    	wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		    	rssfingers = new Rssfingers();//初始化
	
				if (!wifiManager.isWifiEnabled()) 
				{  
			        wifiManager.setWifiEnabled(true); 
			    }    
		        for(int num = 0 ;num<SCANTIMES ;num++)
		        {
		    		list = wifiManager.getScanResults();
		    		wifiManager.startScan();
		        	Thread scanThread = new Thread(new StartScan(mssid));
		        	scanThread.start();
		        	try
		        	{
						Thread.sleep(1000);
					} 
		        	catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	try 
		        	{
						scanThread.join();
					} catch (InterruptedException e) 
		        	{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		        rssfingers.pointId = Integer.parseInt(strinputID);
		        Log.v("Myy",String.valueOf(rssfingers.addrRss));
		        sendData.add(rssfingers);
		        //rssfingers.pointId = 0;
		        //rssfingers.addrRss.clear();  
//		        ListView listView = (ListView) findViewById(R.id.listView);   
//		        listView.setAdapter(new MyAdapter(this,list));
				break;
			case R.id.upload:
		        ip="10.13.32.141";
		        port = 8888;
				new Thread(new Runnable(){  
	                @Override  
	                public void run() {  
	    		        try
	    		        {
	    			        server.connectTheServer(ip,port);
	    			        fingerId = server.sendTheData(sendData);
	    			        Log.v("Myy","上传"+fingerId);
	    			        if(fingerId != null && fingerId != "")
	    			        	handler.sendEmptyMessage(1);
	    			        else
	    			        	handler.sendEmptyMessage(-1);
	    		        }
	    		        catch(Exception e)
	    		        {
	    		        	e.printStackTrace();
	    		        	handler.sendEmptyMessage(-2);
	    		        }  
	                }  
				}).start(); 
				//inputID.setText(Integer.parseInt(inputID.getText().toString().trim())+1);
				Log.v("pointid",Integer.parseInt(inputID.getText().toString().trim())+1+"");
				
				Log.v("Myy",String.valueOf(sendData));				
				break;
			case R.id.clear:
					sendData.clear();
					break;
		}
	}
    
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what == 1) {

				Alert.ShowToast(WifiListActivity.this, "上传完成并清空本地数据", R.drawable.duihao);
				inputID.setText(Integer.parseInt(inputID.getText().toString().trim())+1+"");
				Alert.ShowToast(WifiListActivity.this, "将要收集第"+Integer.parseInt(inputID.getText().toString().trim())+"个位置数据", 1);
				sendData.clear();
				fingerId = null;
			}		
			if (msg.what == -1) {
				Alert.ShowToast(WifiListActivity.this, "返回fingerId错误", R.drawable.cuohao);
			}
			if (msg.what == -2) {
				Alert.ShowToast(WifiListActivity.this, "上传失败", R.drawable.cuohao);
			}
		}
	};
    
    
    
    
    
    
           /* listView.setOnItemClickListener(new OnItemClickListener() {  
            	  
                @Override  
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,  
                        long arg3) {  
                    setTitle("点击第"+arg2+"个项目");  
                }

            });  */ 
      
//  
//    public class MyAdapter extends BaseAdapter {  
//  
//        LayoutInflater inflater;  
//        List<ScanResult> list;  
//        public MyAdapter(Context context, List<ScanResult> list) {  
//            // TODO Auto-generated constructor stub  
//            this.inflater = LayoutInflater.from(context);  
//            this.list = list;  
//        }  
//  
//        @Override  
//        public int getCount() {  
//            // TODO Auto-generated method stub  
//            return list.size();  
//        }  
//  
//        @Override  
//        public Object getItem(int position) {  
//            // TODO Auto-generated method stub  
//            return position;  
//        }  
//  
//        @Override  
//        public long getItemId(int position) {  
//            // TODO Auto-generated method stub  
//            return position;  
//        }  
//  
//        @Override  
//        public View getView(int position, View convertView, ViewGroup parent)
//        {  
//            // TODO Auto-generated method stub 
//        	/*
//       getBSSID() 获取BSSID
//       getDetailedStateOf() 获取客户端的连通性
//       getHiddenSSID() 获得SSID 是否被隐藏
//       getIpAddress() 获取IP 地址
//       getLinkSpeed() 获得连接的速度
//       getMacAddress() 获得Mac 地址
//       getRssi() 获得802.11n 网络的信号
//       getSSID() 获得SSID
//       getSupplicanState() 返回具体客户端状态的信息
//
//        	 */
//            View view = null;  
//            view = inflater.inflate(R.layout.item_wifi_list, null);  
//            ScanResult scanResult = list.get(position);
//            //TextView textView = (TextView) view.findViewById(R.id.textView);  
//            //textView.setText(scanResult.SSID);  自己注释
//            TextView signalStrenth = (TextView) view.findViewById(R.id.signal_strenth);
//            //signalStrenth.setText(String.valueOf(Math.abs(scanResult.level)));
//            
//             
//            StringBuffer mStringBuffer = null;
//            mStringBuffer = new StringBuffer(); 
//            //level 转换rssi 说明http://www.zhihu.com/question/21106590
//            //RSSI = level - NoiceFloor 
//            //NoiceFloor一般取-96dBm
//            //这样如果 level 是 -60dBm, RSSI 就是 36
//            mStringBuffer = mStringBuffer.append("NO.").append(position + 1)   
//                    .append("\nSSID:").append(scanResult.SSID).append("\nBSSID：")   
//                    .append(scanResult.BSSID).append(" \n接入性能：")   
//                    .append(scanResult.capabilities).append("\n频率：")   
//                    .append(scanResult.frequency).append("\nRSSI：").append(Rssfingers.addrRss.get(scanResult.BSSID)+"\n");
//            signalStrenth.setText(mStringBuffer.toString());
//            //mLabelWifi.setText(mStringBuffer.toString());
//            System.out.println(mStringBuffer.toString());
//            return view;  
//        }
// 
//    }  
  
} 

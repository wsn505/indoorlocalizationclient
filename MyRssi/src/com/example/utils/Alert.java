package com.example.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;




public class Alert {
	public static void ShowToast(Context context,String msg,int img){
		Toast toast = Toast.makeText(context,
			     msg, 1000);
		 	LinearLayout toastView = (LinearLayout) toast.getView();
		 	ImageView imageCodeProject = new ImageView(context);
		   imageCodeProject.setImageResource(img);
		   toastView.addView(imageCodeProject, 0);
			   toast.setGravity(Gravity.CENTER, 0, 0);
			   toast.show();
	}

	

}

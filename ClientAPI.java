/* 
 * 2014-11-07
 * Author: Cholerae 
 * Email: choleraehyq@gmail.com
 */

import java.net.*;
import java.io.*;
import java.util.*;

public class ClientAPI {
	private Socket socket = null;
	//private static final int port = 8000;
	private ObjectOutputStream toServer = null;
	private BufferedReader fromServer =null;
	
	public void connectTheServer(String serverIP, int port) {
		try {
			socket = new Socket(serverIP, port);
			toServer = new ObjectOutputStream(socket.getOutputStream());
			fromServer = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	//@SuppressWarnings("finally")
	public String sendTheData(ArrayList<Protocol> sendData) {
		String recvBuf = null;
		try {
			for (Protocol send: sendData) {
				toServer.writeObject(send);
			}
			Protocol endOfSend = new Protocol(-2);
			toServer.writeObject(endOfSend);
			recvBuf = fromServer.readLine(); 
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		finally {
			return recvBuf;
		}
	}
	
	public void closeTheConnection() {
		try {
			fromServer.close();
			toServer.close();
			socket.close();		
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
